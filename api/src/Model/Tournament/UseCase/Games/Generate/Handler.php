<?php


namespace App\Model\Tournament\UseCase\Games\Generate;


use App\Model\Flusher;
use App\Model\Tournament\Entity\Command\CommandRepository;
use App\Model\Tournament\Entity\Division\DivisionRepository;
use App\Model\Tournament\Entity\Division\Id;
use App\Model\Tournament\Entity\Game\Game;
use App\Model\Tournament\Entity\GameToCommand\GameToCommand;
use App\Model\Tournament\Entity\GameToCommand\GameToCommandRepository;
use App\Model\Tournament\Entity\GameType\GameType;
use App\Model\Tournament\Entity\GameType\GameTypeRepository;
use App\ReadModel\Tournament\CommandFetcher;

class Handler
{
    private $commandFetcher;
    private $flusher;
    private $commandRepository;
    private $gameTypeRepository;
    private $repository;

    public function __construct(Flusher $flusher, CommandFetcher $commandFetcher, CommandRepository $commandRepository, GameTypeRepository $gameTypeRepository, GameToCommandRepository  $repository)
    {
        $this->commandFetcher = $commandFetcher;
        $this->flusher = $flusher;
        $this->commandRepository = $commandRepository;
        $this->gameTypeRepository = $gameTypeRepository;
        $this->repository = $repository;
    }

    public function handle(Command $command): void
    {
        $commandList = $this->commandFetcher->listOfCommandsByDivision($command->id);
        $date = new \DateTimeImmutable();
        $gameType = $this->gameTypeRepository->getBy(['name' => 'regular game']);

        foreach ($commandList as $item){
            foreach ($commandList as $value){
                if ($item['id'] != $value['id']){
                    $tournamentGame = Game::create($date);
                    $gameToCommand1 = $this->createGame($item, $gameType, $tournamentGame);
                    $this->repository->add($gameToCommand1);

                    $gameToCommand2 = $this->createGame($value, $gameType, $tournamentGame);
                    $this->repository->add($gameToCommand2);
                }
            }
        }

        $this->flusher->flush();
    }

    public function createGame(array $item, GameType $gameType, Game $tournamentGame)
    {
        $command = $this->commandRepository->get(new \App\Model\Tournament\Entity\Command\Id($item['id']));
        $goals = random_int(0, 5);
        $gameToCommand = GameToCommand::create($tournamentGame, $command, $gameType, $goals);
        return $gameToCommand;
    }
}