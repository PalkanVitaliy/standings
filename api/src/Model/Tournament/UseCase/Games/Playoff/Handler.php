<?php


namespace App\Model\Tournament\UseCase\Games\Playoff;


use App\Helpers\SortHelper;
use App\Model\Flusher;
use App\Model\Tournament\Entity\Command\CommandRepository;
use App\Model\Tournament\Entity\Division\DivisionRepository;
use App\Model\Tournament\Entity\Division\Id;
use App\Model\Tournament\Entity\Game\Game;
use App\Model\Tournament\Entity\GameToCommand\GameToCommand;
use App\Model\Tournament\Entity\GameToCommand\GameToCommandRepository;
use App\Model\Tournament\Entity\GameType\GameType;
use App\Model\Tournament\Entity\GameType\GameTypeRepository;
use App\ReadModel\Tournament\CommandFetcher;
use App\ReadModel\Tournament\TournamentFetcher;

class Handler
{
    private $flusher;
    private $tournamentFetcher;
    private $gameTypeRepository;
    private $divisionRepository;
    private $helper;
    private $commandRepository;
    private $repository;

    public function __construct(Flusher $flusher, TournamentFetcher $tournamentFetcher, GameTypeRepository $gameTypeRepository, DivisionRepository $divisionRepository, SortHelper $helper, CommandRepository $commandRepository, GameToCommandRepository $repository)
    {
        $this->flusher = $flusher;
        $this->tournamentFetcher = $tournamentFetcher;
        $this->gameTypeRepository = $gameTypeRepository;
        $this->divisionRepository = $divisionRepository;
        $this->helper = $helper;
        $this->commandRepository = $commandRepository;
        $this->repository = $repository;
    }

    public function handle(): void
    {
        $gameType = $this->gameTypeRepository->getBy(['name' => 'regular game']);
        $division1 = $this->divisionRepository->getBy(['name' => 'division_A']);
        $division2 = $this->divisionRepository->getBy(['name' => 'division_B']);

        $resultA = $this->tournamentFetcher->listOfCommands($gameType->getId()->getValue(), $division1->getId()->getValue());
        $this->helper->sort($resultA);
        $resultB = $this->tournamentFetcher->listOfCommands($gameType->getId()->getValue(), $division2->getId()->getValue());
        $this->helper->sort($resultB);

        $gameType = $this->gameTypeRepository->getBy(['name' => 'quarter final']);
        $command_quater_1 = $this->createGameToCommand($resultA[0]['command_id'], $resultB[3]['command_id'], $gameType);
        $command_quater_2 = $this->createGameToCommand($resultA[1]['command_id'], $resultB[2]['command_id'], $gameType);
        $command_quater_3 = $this->createGameToCommand($resultA[2]['command_id'], $resultB[1]['command_id'], $gameType);
        $command_quater_4 = $this->createGameToCommand($resultA[3]['command_id'], $resultB[0]['command_id'], $gameType);

        $gameTypeSemi = $this->gameTypeRepository->getBy(['name' => 'semi final']);
        $command_semi_1 = $this->createGameToCommand($command_quater_1['win'], $command_quater_2['win'], $gameTypeSemi);
        $command_semi_2 = $this->createGameToCommand($command_quater_3['win'], $command_quater_4['win'], $gameTypeSemi);

        $gameTypeFinalThird = $this->gameTypeRepository->getBy(['name' => 'third place game']);
        $this->createGameToCommand($command_semi_1['lose'], $command_semi_2['lose'], $gameTypeFinalThird);

        $gameTypeFinal = $this->gameTypeRepository->getBy(['name' => 'final']);
        $this->createGameToCommand($command_semi_1['win'], $command_semi_2['win'], $gameTypeFinal);
        $this->flusher->flush();
    }

    private function createGameToCommand(int $command_1, int $command_2, GameType $gameType): array
    {
        $date = new \DateTimeImmutable();
        $tournamentGame = Game::create($date);
        $gameToCommand1 = $this->createGame($command_1, $gameType, $tournamentGame, 2);
        $this->repository->add($gameToCommand1);

        $gameToCommand2 = $this->createGame($command_2, $gameType, $tournamentGame, 1);
        $this->repository->add($gameToCommand2);

        return [
            'win' => $command_1,
            'lose' => $command_2
        ];
    }

    private function createGame($command_id, GameType $gameType, Game $tournamentGame, $goals): GameToCommand
    {
        $command = $this->commandRepository->get(new \App\Model\Tournament\Entity\Command\Id($command_id));
        $gameToCommand = GameToCommand::create($tournamentGame, $command, $gameType, $goals);
        return $gameToCommand;
    }
}