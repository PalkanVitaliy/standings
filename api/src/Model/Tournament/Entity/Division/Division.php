<?php
declare(strict_types=1);

namespace App\Model\Tournament\Entity\Division;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tournament_division")
 */
class Division
{
    /**
     * @var Id
     * @ORM\Column(type="tournament_division_id")
     * @ORM\GeneratedValue()
     * @ORM\Id
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    public static function create($name)
    {
        $division = new self();
        $division->name = $name;
        return $division;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): Id
    {
        return $this->id;
    }
}