<?php
declare(strict_types=1);

namespace App\Model\Tournament\Entity\Division;

use Doctrine\ORM\EntityManagerInterface;
use App\Model\EntityNotFoundException;

class DivisionRepository
{
    private $em;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Division::class);
    }

    public function add(Division $division): void
    {
        $this->em->persist($division);
    }

    public function get(Id $id): Division
    {
        if (!$division = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Division is not found.');
        }
        return $division;
    }

    public function getBy($credentials): Division
    {
        if (!$division = $this->repo->findOneBy($credentials)) {
            throw new EntityNotFoundException('Division is not found.');
        }
        return $division;
    }
}