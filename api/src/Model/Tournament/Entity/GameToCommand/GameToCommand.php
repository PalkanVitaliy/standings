<?php


namespace App\Model\Tournament\Entity\GameToCommand;


use App\Model\Tournament\Entity\Command\Command;
use App\Model\Tournament\Entity\Game\Game;
use App\Model\Tournament\Entity\GameType\GameType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="game_to_command")
 */
class GameToCommand
{
    /**
     * @var Id
     * @ORM\Column(type="game_to_command_id")
     * @ORM\GeneratedValue()
     * @ORM\Id
     */
    private $id;
    /**
     * @var Game
     * @ORM\ManyToOne(targetEntity="App\Model\Tournament\Entity\Game\Game", inversedBy="gamesToCommand", cascade={"persist"})
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id", nullable=false)
     */
    private $game;
    /**
     * @var Command
     * @ORM\ManyToOne(targetEntity="App\Model\Tournament\Entity\Command\Command", inversedBy="gamesToCommand", cascade={"persist"})
     * @ORM\JoinColumn(name="command_id", referencedColumnName="id", nullable=false)
     */
    private $command;
    /**
     * @var GameType
     * @ORM\ManyToOne(targetEntity="App\Model\Tournament\Entity\GameType\GameType", inversedBy="gamesToCommand", cascade={"persist"})
     * @ORM\JoinColumn(name="game_type_id", referencedColumnName="id", nullable=false)
     */
    private $gameType;
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $goals;

    public static function create(Game $game, Command $command, GameType $gameType, $goals): self
    {
        $gameToCommand = new self();
        $gameToCommand->game = $game;
        $gameToCommand->command = $command;
        $gameToCommand->gameType = $gameType;
        $gameToCommand->goals = $goals;
        return $gameToCommand;
    }

    public function getGoals(): int
    {
        return  $this->goals;
    }

    public function getGameType(): GameType
    {
        return  $this->gameType;
    }

    public function getCommand(): Command
    {
        return  $this->command;
    }

    public function getGame(): Game
    {
        return  $this->game;
    }

}