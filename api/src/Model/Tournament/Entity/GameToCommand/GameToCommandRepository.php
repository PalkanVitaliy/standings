<?php


namespace App\Model\Tournament\Entity\GameToCommand;


use App\Model\EntityNotFoundException;
use Doctrine\ORM\EntityManagerInterface;

class GameToCommandRepository
{
    private $em;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(GameToCommand::class);
    }

    public function add(GameToCommand $gameTocommand): void
    {
        $this->em->persist($gameTocommand);
    }

    public function get(Id $id): GameToCommand
    {
        if (!$gameTocommand = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('GameToCommand is not found.');
        }
        return $gameTocommand;
    }
}