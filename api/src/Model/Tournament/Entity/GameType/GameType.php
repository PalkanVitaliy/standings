<?php
declare(strict_types=1);

namespace App\Model\Tournament\Entity\GameType;

use App\Model\Tournament\Entity\GameToCommand\GameToCommand;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="game_type")
 */
class GameType
{
    /**
     * @var Id
     * @ORM\Column(type="game_type_id")
     * @ORM\GeneratedValue()
     * @ORM\Id
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @var ArrayCollection|GameToCommand[]
     * @ORM\OneToMany(targetEntity="App\Model\Tournament\Entity\GameToCommand\GameToCommand", mappedBy="game", orphanRemoval=true, cascade={"all"})
     */
    private $gamesToCommand;

    public static function create($name)
    {
        $division = new self();
        $division->name = $name;
        return $division;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): Id
    {
        return $this->id;
    }
}