<?php


namespace App\Model\Tournament\Entity\GameType;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\EntityNotFoundException;

class GameTypeRepository
{
    private $em;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(GameType::class);
    }

    public function add(GameType $gameType): void
    {
        $this->em->persist($gameType);
    }

    public function get(Id $id): GameType
    {
        if (!$gameType = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('GameType is not found.');
        }
        return $gameType;
    }

    public function getBy($credentials): GameType
    {
        if (!$gameType = $this->repo->findOneBy($credentials)) {
            throw new EntityNotFoundException('GameType is not found.');
        }
        return $gameType;
    }
}