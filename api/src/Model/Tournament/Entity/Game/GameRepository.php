<?php


namespace App\Model\Tournament\Entity\Game;


use App\Model\EntityNotFoundException;
use Doctrine\ORM\EntityManagerInterface;

class GameRepository
{
    private $em;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Game::class);
    }

    public function add(Game $game): void
    {
        $this->em->persist($game);
    }

    public function get(Id $id): Game
    {
        if (!$game = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Game is not found.');
        }
        return $game;
    }
}