<?php
declare(strict_types=1);

namespace App\Model\Tournament\Entity\Game;
use App\Model\Tournament\Entity\GameToCommand\GameToCommand;
use App\Model\Tournament\Entity\GameType\Id;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tournament_game")
 */
class Game
{
    /**
     * @var Id
     * @ORM\Column(type="tournament_game_id")
     * @ORM\GeneratedValue()
     * @ORM\Id
     */
    private $id;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;
    /**
     * @var ArrayCollection|GameToCommand[]
     * @ORM\OneToMany(targetEntity="App\Model\Tournament\Entity\GameToCommand\GameToCommand", mappedBy="game", orphanRemoval=true, cascade={"all"})
     */
    private $gamesToCommand;

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public static function create(\DateTimeImmutable $date): self
    {
        $game = new self();
        $game->date = $date;
        return $game;
    }
}