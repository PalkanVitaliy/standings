<?php
declare(strict_types=1);

namespace App\Model\Tournament\Entity\Command;

use App\Model\Tournament\Entity\Division\Division;
use App\Model\Tournament\Entity\GameToCommand\GameToCommand;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tournament_command")
 */
class Command
{
    /**
     * @var Id
     * @ORM\Column(type="tournament_division_id")
     * @ORM\GeneratedValue()
     * @ORM\Id
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @var Division
     * @ORM\ManyToOne(targetEntity="App\Model\Tournament\Entity\Division\Division")
     * @ORM\JoinColumn(name="tournament_division_id", referencedColumnName="id", nullable=false)
     */
    private $tournament_division;
    /**
     * @var ArrayCollection|GameToCommand[]
     * @ORM\OneToMany(targetEntity="App\Model\Tournament\Entity\GameToCommand\GameToCommand", mappedBy="game", orphanRemoval=true, cascade={"all"})
     */
    private $gamesToCommand;

    public function getId(): Id
    {
        return $this->id;
    }

    public function getDivision(): Division
    {
        return $this->tournament_division;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public static function create($name, Division $division): self
    {
        $command = new self();
        $command->name = $name;
        $command->tournament_division = $division;
        return $command;
    }
}