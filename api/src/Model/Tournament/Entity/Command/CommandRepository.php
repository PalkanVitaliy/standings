<?php
declare(strict_types=1);

namespace App\Model\Tournament\Entity\Command;

use Doctrine\ORM\EntityManagerInterface;
use App\Model\EntityNotFoundException;

class CommandRepository
{
    private $em;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Command::class);
    }

    public function add(Command $command): void
    {
        $this->em->persist($command);
    }

    public function get(Id $id): Command
    {
        if (!$command = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Command is not found.');
        }
        return $command;
    }
}