<?php
declare(strict_types=1);

namespace App\ReadModel\Tournament;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

class GameFetcher
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getFinalCommand($id): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'gc.command_id'
            )
            ->from('tournament_game', 'h')
            ->innerJoin('h', 'game_to_command', 'gc', 'h.id = gc.game_id')
            ->andWhere('game_type_id = :id')
            ->setParameter('id', $id)
            ->execute();

        return array_column($stmt->fetchAll(FetchMode::ASSOCIATIVE), 'command_id');
    }

    public function getGamePlayoff($id): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'gc.game_id', 'gc.command_id', 'gc.goals', 'gc.game_type_id', 'tc.name', 'gt.name as gt_name'
            )
            ->from('tournament_game', 'h')
            ->leftJoin('h', 'game_to_command', 'gc', 'h.id = gc.game_id')
            ->leftJoin('gc', 'tournament_command', 'tc', 'gc.command_id = tc.id')
            ->leftJoin('gc', 'game_type', 'gt', 'gc.game_type_id = gt.id')
            ->andWhere('game_type_id <> :id')
            ->setParameter('id', $id)
            ->orderBy('gc.game_type_id', 'desc')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}