<?php
declare(strict_types=1);

namespace App\ReadModel\Tournament;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

class CommandFetcher
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function listOfCommands(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'name'
            )
            ->from('tournament_command')
            ->orderBy('name')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }

    public function listOfCommandsByDivision($id): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'name'
            )
            ->from('tournament_command')
            ->where('tournament_division_id = :id')
            ->setParameter('id', $id)
            ->orderBy('name')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}