<?php


namespace App\ReadModel\Tournament;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

class TournamentFetcher
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function listOfCommands($id, $division_id): array
    {
       $stmt = $this->connection->prepare('SELECT game_to_command.command_id                                  as command_id, tournament_command.name                                     as command_name,
       SUM(
               CASE
                   WHEN game_to_command.goals = cp.goals THEN 1
                   WHEN game_to_command.goals > cp.goals THEN 3
                   ELSE 0
                   END
           )                                                       AS total,
       SUM(CASE WHEN game_to_command.goals = cp.goals THEN 1 ELSE 0 END) as sum_draw,
       SUM(CASE WHEN game_to_command.goals > cp.goals THEN 1 ELSE 0 END) as sum_win,
       SUM(CASE WHEN game_to_command.goals < cp.goals THEN 1 ELSE 0 END) as sum_defeat,
       SUM(game_to_command.goals)                             as goals_scored,
       SUM(cp.goals)                                          as goals_conceded,
       count(game_to_command.game_id)                         as total_game
FROM game_to_command
         LEFT JOIN (SELECT game_id, command_id, goals FROM game_to_command) as cp
ON cp.game_id = game_to_command.game_id
         LEFT JOIN tournament_command
                   ON tournament_command.id = game_to_command.command_id
WHERE game_to_command.command_id <> cp.command_id AND game_to_command.game_type_id = :type AND tournament_command.tournament_division_id = :division GROUP BY game_to_command.command_id, command_name');
        $stmt->bindValue("type", $id);
        $stmt->bindValue("division", $division_id);
        $stmt->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }

    public function listOfCommandsPlayoff($id): array
    {
        $stmt = $this->connection->prepare('SELECT game_to_command.command_id                                  as command_id,
       tournament_command.name                                     as command_name,
       SUM(CASE WHEN game_to_command.goals = cp.goals THEN 1 ELSE 0 END) as sum_draw,
       SUM(CASE WHEN game_to_command.goals > cp.goals THEN 1 ELSE 0 END) as sum_win,
       SUM(CASE WHEN game_to_command.goals < cp.goals THEN 1 ELSE 0 END) as sum_defeat,
       SUM(game_to_command.goals)                             as goals_scored,
       SUM(cp.goals)                                          as goals_conceded,
       count(game_to_command.game_id)                         as total_game
FROM game_to_command
         LEFT JOIN (SELECT game_id, command_id, goals FROM game_to_command) as cp
ON cp.game_id = game_to_command.game_id
         LEFT JOIN tournament_command
                   ON tournament_command.id = game_to_command.command_id
WHERE game_to_command.command_id <> cp.command_id AND game_to_command.game_type_id <> :type GROUP BY game_to_command.command_id, command_name');
        $stmt->bindValue("type", $id);
        $stmt->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}