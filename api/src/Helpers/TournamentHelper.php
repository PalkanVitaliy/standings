<?php


namespace App\Helpers;


class TournamentHelper
{
    public static function transform(array $games)
    {
        $newGamesArray = [];
        foreach ($games as $game){
            if (!array_key_exists($game['game_id'], $newGamesArray)){
                $newGamesArray[$game['game_id']]['command_1'] = $game['command_id'];
                $newGamesArray[$game['game_id']]['command_name_1'] = $game['name'];
                $newGamesArray[$game['game_id']]['goals_1'] = $game['goals'];
                $newGamesArray[$game['game_id']]['game_type'] = $game['game_type_id'];
                $newGamesArray[$game['game_id']]['game_type_name'] = $game['gt_name'];
            } else {
                $newGamesArray[$game['game_id']]['command_2'] = $game['command_id'];
                $newGamesArray[$game['game_id']]['goals_2'] = $game['goals'];
                $newGamesArray[$game['game_id']]['command_name_2'] = $game['name'];
            }

        }
        return $newGamesArray;
    }
}