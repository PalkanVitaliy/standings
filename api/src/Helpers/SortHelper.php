<?php
declare(strict_types=1);

namespace App\Helpers;


class SortHelper
{
    public function sort(&$result)
    {
        usort($result, function($a, $b){
            $retval = $b['total'] <=> $a['total'];
            if ($retval == 0) {
                $sumB = $b['goals_scored'] - $b['goals_conceded'];
                $sumA = $a['goals_scored'] - $a['goals_conceded'];
                $retval = $sumB <=> $sumA;
            }
            return $retval;
        });
    }

    public function sortPlayoff(&$result, array $finalCommand)
    {
        usort($result, function($a, $b) use ($finalCommand){
            $retval = $b['sum_win'] <=> $a['sum_win'];
            if ($retval == 0) {
                if (in_array($b['command_id'], $finalCommand)){
                    return 1;
                } elseif (in_array($a['command_id'], $finalCommand)){
                    return -1;
                } else{
                    $sumB = $b['goals_scored'] - $b['goals_conceded'];
                    $sumA = $a['goals_scored'] - $a['goals_conceded'];
                    $retval = $sumB <=> $sumA;
                }
            }
            return $retval;
        });
    }
}