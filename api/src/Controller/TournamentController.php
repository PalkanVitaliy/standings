<?php
declare(strict_types=1);

namespace App\Controller;

use App\Helpers\SortHelper;
use App\Helpers\TournamentHelper;
use App\Model\Tournament\Entity\Division\DivisionRepository;
use App\Model\Tournament\Entity\GameType\GameTypeRepository;
use App\ReadModel\Tournament\CommandFetcher;
use App\ReadModel\Tournament\GameFetcher;
use App\ReadModel\Tournament\TournamentFetcher;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/tournament", name="tournament")
 */
class TournamentController  extends AbstractController
{
    private $errors;
    private $gameTypeRepository;
    private $divisionRepository;
    private $gameFetcher;

    public function __construct(ErrorHandler $errors, GameTypeRepository $gameTypeRepository, DivisionRepository $divisionRepository, GameFetcher $gameFetcher)
    {
        $this->errors = $errors;
        $this->gameTypeRepository = $gameTypeRepository;
        $this->divisionRepository = $divisionRepository;
        $this->gameFetcher = $gameFetcher;
    }
    /**
     * @Route("/A", name=".A")
     * @param TournamentFetcher $tournamentFetcher
     * @return Response
     */
    public function divisionA(TournamentFetcher $tournamentFetcher, SortHelper $helper): Response
    {
        $gameType = $this->gameTypeRepository->getBy(['name' => 'regular game']);
        $division = $this->divisionRepository->getBy(['name' => 'division_A']);

        $result = $tournamentFetcher->listOfCommands($gameType->getId()->getValue(), $division->getId()->getValue());
        $helper->sort($result);

        return $this->render('app/tournament/command/division/index.html.twig', [
            'commands' => $result,
        ]);
    }

    /**
     * @Route("/B", name=".B")
     * @param TournamentFetcher $tournamentFetcher
     * @return Response
     */
    public function divisionB(TournamentFetcher $tournamentFetcher, SortHelper $helper): Response
    {
        $gameType = $this->gameTypeRepository->getBy(['name' => 'regular game']);
        $division = $this->divisionRepository->getBy(['name' => 'division_B']);

        $result = $tournamentFetcher->listOfCommands($gameType->getId()->getValue(), $division->getId()->getValue());
        $helper->sort($result);

        return $this->render('app/tournament/command/division/index.html.twig', [
            'commands' => $result,
        ]);
    }

    /**
     * @Route("/playoff", name=".playoff")
     * @param TournamentFetcher $tournamentFetcher
     * @return Response
     */
    public function playoff(TournamentFetcher $tournamentFetcher, SortHelper $helper): Response
    {
        $gameType = $this->gameTypeRepository->getBy(['name' => 'regular game']);
        $gameFinalType = $this->gameTypeRepository->getBy(['name' => 'final']);

        $result = $tournamentFetcher->listOfCommandsPlayoff($gameType->getId()->getValue());

        $finalCommand = $this->gameFetcher->getFinalCommand($gameFinalType->getId()->getValue());

        $helper->sortPlayoff($result, $finalCommand);

        return $this->render('app/tournament/command/playoff/index.html.twig', [
            'commands' => $result,
        ]);
    }

    /**
     * @Route("/games", name=".games")
     * @param TournamentFetcher $tournamentFetcher
     * @return Response
     */
    public function games(TournamentFetcher $tournamentFetcher, SortHelper $helper): Response
    {
        $gameType = $this->gameTypeRepository->getBy(['name' => 'regular game']);


        $finalCommand = $this->gameFetcher->getGamePlayoff($gameType->getId()->getValue());
        $result = TournamentHelper::transform($finalCommand);

        return $this->render('app/tournament/command/games/index.html.twig', [
            'games' => $result,
        ]);
    }
}