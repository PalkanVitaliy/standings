<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Tournament\Entity\Division\Division;
use App\ReadModel\Tournament\CommandFetcher;
use App\ReadModel\Tournament\DivisionFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Model\Tournament\UseCase\Games\Generate;
use App\Model\Tournament\UseCase\Games\Playoff;

/**
 * @Route("/", name="command")
 */
class CommandController  extends AbstractController
{
    private $errors;
    private $divisionFetcher;

    public function __construct(ErrorHandler $errors, DivisionFetcher $divisionFetcher)
    {
        $this->errors = $errors;
        $this->divisionFetcher = $divisionFetcher;
    }
    /**
     * @Route("", name="")
     * @param CommandFetcher $stays_type
     * @return Response
     */
    public function index(CommandFetcher $commandFetcher): Response
    {
        $divisions = $this->divisionFetcher->listOfCommands();

        return $this->render('app/tournament/command/index.html.twig', [
            'commands' => $commandFetcher->listOfCommands(),
            'divisions' => $divisions
        ]);
    }

    /**
     * @Route("/generate/{id}", name=".generate", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function generate(Division $division, Request $request, Generate\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('generate', $request->request->get('token'))) {
            return $this->redirectToRoute('command');
        }

        $command = new Generate\Command($division->getId()->getValue());

        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('command');
    }

    /**
     * @Route("/playoff", name=".playoff", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function playoff(Request $request, Playoff\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('generate', $request->request->get('token'))) {
            return $this->redirectToRoute('command');
        }

        try {
            $handler->handle();
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('command');
    }
}