<?php


namespace App\DataFixtures;

use App\Model\Tournament\Entity\Division\Division;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DivisionFixture extends Fixture
{
    public const REFERENCE_A = 'division_A';
    public const REFERENCE_B = 'division_B';

    public function load(ObjectManager $manager): void
    {
        $A = Division::create("division_A");
        $manager->persist($A);
        $this->setReference(self::REFERENCE_A, $A);


        $B = Division::create("division_B");
        $manager->persist($B);
        $this->setReference(self::REFERENCE_B, $B);

        $manager->flush();
    }
}