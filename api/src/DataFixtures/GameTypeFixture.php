<?php


namespace App\DataFixtures;


use App\Model\Tournament\Entity\GameType\GameType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GameTypeFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $regular_game = GameType::create("regular game");
        $manager->persist($regular_game);

        $quater_final = GameType::create("quarter final");
        $manager->persist($quater_final);

        $semi_final = GameType::create("semi final");
        $manager->persist($semi_final);

        $third_place_game = GameType::create("third place game");
        $manager->persist($third_place_game);

        $final = GameType::create("final");
        $manager->persist($final);

        $manager->flush();
    }
}