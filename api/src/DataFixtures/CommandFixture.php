<?php


namespace App\DataFixtures;
use App\Model\Tournament\Entity\Command\Command;
use App\Model\Tournament\Entity\Division\Division;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommandFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /**
         * @var Division $A
         * @var Division $B
         */
        $A = $this->getReference(DivisionFixture::REFERENCE_A);
        $B = $this->getReference(DivisionFixture::REFERENCE_B);


        $command1 = Command::create("command_A", $A);
        $manager->persist($command1);
        $command2 = Command::create("command_B", $A);
        $manager->persist($command2);
        $command3 = Command::create("command_C", $A);
        $manager->persist($command3);
        $command4 = Command::create("command_D", $A);
        $manager->persist($command4);
        $command5 = Command::create("command_E", $A);
        $manager->persist($command5);
        $command6 = Command::create("command_F", $A);
        $manager->persist($command6);
        $command7 = Command::create("command_G", $A);
        $manager->persist($command7);
        $command8 = Command::create("command_H", $A);
        $manager->persist($command8);

        $command9 = Command::create("command_I", $B);
        $manager->persist($command9);
        $command10 = Command::create("command_J", $B);
        $manager->persist($command10);
        $command11 = Command::create("command_K", $B);
        $manager->persist($command11);
        $command12 = Command::create("command_L", $B);
        $manager->persist($command12);
        $command13 = Command::create("command_M", $B);
        $manager->persist($command13);
        $command14 = Command::create("command_N", $B);
        $manager->persist($command14);
        $command15 = Command::create("command_O", $B);
        $manager->persist($command15);
        $command16 = Command::create("command_P", $B);
        $manager->persist($command16);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            DivisionFixture::class
        ];
    }
}