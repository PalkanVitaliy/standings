<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126111322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE game_to_command_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE game_to_command (id INT NOT NULL, game_id INT NOT NULL, command_id INT NOT NULL, game_type_id INT NOT NULL, goals INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EE8E10CCE48FD905 ON game_to_command (game_id)');
        $this->addSql('CREATE INDEX IDX_EE8E10CC33E1689A ON game_to_command (command_id)');
        $this->addSql('CREATE INDEX IDX_EE8E10CC508EF3BC ON game_to_command (game_type_id)');
        $this->addSql('COMMENT ON COLUMN game_to_command.id IS \'(DC2Type:game_to_command_id)\'');
        $this->addSql('COMMENT ON COLUMN game_to_command.game_id IS \'(DC2Type:tournament_game_id)\'');
        $this->addSql('COMMENT ON COLUMN game_to_command.command_id IS \'(DC2Type:tournament_division_id)\'');
        $this->addSql('COMMENT ON COLUMN game_to_command.game_type_id IS \'(DC2Type:game_type_id)\'');
        $this->addSql('ALTER TABLE game_to_command ADD CONSTRAINT FK_EE8E10CCE48FD905 FOREIGN KEY (game_id) REFERENCES tournament_game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_to_command ADD CONSTRAINT FK_EE8E10CC33E1689A FOREIGN KEY (command_id) REFERENCES tournament_command (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_to_command ADD CONSTRAINT FK_EE8E10CC508EF3BC FOREIGN KEY (game_type_id) REFERENCES game_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE game_to_command_id_seq CASCADE');
        $this->addSql('DROP TABLE game_to_command');
    }
}
