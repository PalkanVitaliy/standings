<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126084825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE tournament_command_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE tournament_command (id INT NOT NULL, tournament_division_id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E5C511DECA514AC3 ON tournament_command (tournament_division_id)');
        $this->addSql('COMMENT ON COLUMN tournament_command.id IS \'(DC2Type:tournament_division_id)\'');
        $this->addSql('COMMENT ON COLUMN tournament_command.tournament_division_id IS \'(DC2Type:tournament_division_id)\'');
        $this->addSql('ALTER TABLE tournament_command ADD CONSTRAINT FK_E5C511DECA514AC3 FOREIGN KEY (tournament_division_id) REFERENCES tournament_division (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE tournament_command_id_seq CASCADE');
        $this->addSql('DROP TABLE tournament_command');
    }
}
