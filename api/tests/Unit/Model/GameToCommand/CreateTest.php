<?php


namespace App\Tests\Unit\Model\GameToCommand;
use App\Model\Tournament\Entity\Command\Command;
use App\Model\Tournament\Entity\GameToCommand\GameToCommand;
use App\Tests\Builder\Tournament\CommandBuilder;
use App\Tests\Builder\Tournament\DivisionBuilder;
use App\Tests\Builder\Tournament\GameBuilder;
use App\Tests\Builder\Tournament\GameTypeBuilder;
use PHPUnit\Framework\TestCase;

class CreateTest extends TestCase
{
    public function testSuccess(): void
    {
        $division = (new DivisionBuilder())->build();
        $command = (new CommandBuilder())->build($division);
        $gameType = (new GameTypeBuilder())->build();
        $game = (new GameBuilder())->build();

        $gameToCommand = GameToCommand::create(
            $game,
            $command,
            $gameType,
            $goals = 5
        );

        self::assertEquals($goals, $gameToCommand->getGoals());
        self::assertEquals($command, $gameToCommand->getCommand());
        self::assertEquals($gameType, $gameToCommand->getGameType());
        self::assertEquals($game, $gameToCommand->getGame());
    }
}