<?php


namespace App\Tests\Unit\Model\Division;
use App\Model\Tournament\Entity\Command\Command;
use App\Model\Tournament\Entity\Division\Division;
use App\Tests\Builder\Tournament\DivisionBuilder;
use PHPUnit\Framework\TestCase;

class CreateTest extends TestCase
{
    public function testSuccess(): void
    {
        $command = Division::create(
            $name = 'A'
        );

        self::assertEquals($name, $command->getName());
    }
}