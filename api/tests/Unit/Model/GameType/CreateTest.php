<?php


namespace App\Tests\Unit\Model\GameType;
use App\Model\Tournament\Entity\Command\Command;
use App\Model\Tournament\Entity\Division\Division;
use App\Model\Tournament\Entity\GameType\GameType;
use App\Tests\Builder\Tournament\DivisionBuilder;
use PHPUnit\Framework\TestCase;

class CreateTest extends TestCase
{
    public function testSuccess(): void
    {
        $command = GameType::create(
            $name = 'final'
        );

        self::assertEquals($name, $command->getName());
    }
}