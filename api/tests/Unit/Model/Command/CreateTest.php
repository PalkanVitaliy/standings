<?php


namespace App\Tests\Unit\Model\Command;
use App\Model\Tournament\Entity\Command\Command;
use App\Tests\Builder\Tournament\DivisionBuilder;
use PHPUnit\Framework\TestCase;

class CreateTest extends TestCase
{
    public function testSuccess(): void
    {
        $division = (new DivisionBuilder())->build();
        $command = Command::create(
            $name = 'Command_1',
            $division
        );

        self::assertEquals($name, $command->getName());
        self::assertEquals($division, $command->getDivision());
    }
}