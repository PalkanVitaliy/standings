<?php


namespace App\Tests\Unit\Model\Game;
use App\Model\Tournament\Entity\Command\Command;
use App\Model\Tournament\Entity\Division\Division;
use App\Model\Tournament\Entity\Game\Game;
use App\Model\Tournament\Entity\GameType\GameType;
use App\Tests\Builder\Tournament\DivisionBuilder;
use PHPUnit\Framework\TestCase;

class CreateTest extends TestCase
{
    public function testSuccess(): void
    {
        $command = Game::create(
            $date = new \DateTimeImmutable()
        );

        self::assertEquals($date, $command->getDate());
    }
}