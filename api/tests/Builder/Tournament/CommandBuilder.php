<?php


namespace App\Tests\Builder\Tournament;



use App\Model\Tournament\Entity\Command\Command;
use App\Model\Tournament\Entity\Division\Division;

class CommandBuilder
{
    private $name;

    public function __construct()
    {
        $this->name = 'command_1';
    }

    public function withName(string $name): self
    {
        $clone = clone $this;
        $clone->name = $name;
        return $clone;
    }

    public function build(Division $division): Command
    {
        return Command::create($this->name, $division);
    }
}