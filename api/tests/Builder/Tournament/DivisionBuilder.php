<?php


namespace App\Tests\Builder\Tournament;


use App\Model\Tournament\Entity\Division\Division;

class DivisionBuilder
{
    private $name;

    public function __construct()
    {
        $this->name = 'A';
    }

    public function withName(string $name): self
    {
        $clone = clone $this;
        $clone->name = $name;
        return $clone;
    }

    public function build(): Division
    {
        return Division::create($this->name);
    }
}