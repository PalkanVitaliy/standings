<?php


namespace App\Tests\Builder\Tournament;




use App\Model\Tournament\Entity\GameType\GameType;

class GameTypeBuilder
{
    private $name;

    public function __construct()
    {
        $this->name = 'final';
    }

    public function withName(string $name): self
    {
        $clone = clone $this;
        $clone->name = $name;
        return $clone;
    }

    public function build(): GameType
    {
        return GameType::create($this->name);
    }
}