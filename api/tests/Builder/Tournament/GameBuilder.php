<?php


namespace App\Tests\Builder\Tournament;


use App\Model\Tournament\Entity\Game\Game;

class GameBuilder
{
    private $name;

    public function __construct()
    {
        $this->date = new \DateTimeImmutable();
    }

    public function withName(\DateTimeImmutable $date): self
    {
        $clone = clone $this;
        $clone->date = $date;
        return $clone;
    }

    public function build(): Game
    {
        return Game::create($this->date);
    }
}