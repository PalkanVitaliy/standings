init: docker-down-clear docker-up api-migrations api-fixtures

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-up:
	docker-compose up -d

api-migrations:
	docker-compose run --rm api-php-cli php bin/console doctrine:migrations:migrate --no-interaction

api-fixtures:
	docker-compose run --rm api-php-cli php bin/console doctrine:fixtures:load --no-interaction

api-test:
	docker-compose run --rm api-php-cli php bin/phpunit

api-assets-dev:
	docker-compose run --rm api-node npm run dev

api-composer-install:
	docker-compose run --rm api-php-cli composer install


